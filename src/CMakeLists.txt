
SET( SRC_SETUP setup.cpp )
add_executable( SETUP ${SRC_SETUP} ${INCL_HEADERS} )

target_link_libraries( SETUP
    PUBLIC ${OpenCV_LIBS} )

SET( SRC_COMPOSE compose.cpp )
add_executable( COMPOSE ${SRC_COMPOSE} ${INCL_HEADERS} )

target_link_libraries( COMPOSE
    PUBLIC ${OpenCV_LIBS} )

SET( SRC_DITHERED dithered.cpp )
add_executable( DITHERED ${SRC_DITHERED} ${INCL_HEADERS} )

target_link_libraries( DITHERED
    PUBLIC ${OpenCV_LIBS} )