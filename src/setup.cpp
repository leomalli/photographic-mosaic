#include <iostream>
#include <chrono>
#include <random>
#include <algorithm>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#define DST_DATA_SIZE 200
#define DST_DATA_RATIO 1.0f

// Some functions
void resizeIfLarger( cv::Mat &img, const int &max_size);
void resizeIfLarger( cv::Mat &img, const int &max_width, const int &max_height);
void resizeIfLarger( cv::Mat &img, const int &max_width, const float &dst_ratio);

// alows us to get random element from vector easily
template<typename Iter, typename RandomGenerator>
Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

template<typename Iter>
Iter select_randomly(Iter start, Iter end) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return select_randomly(start, end, gen);
}

// Used to compare the tuples in vector
bool compareData(std::tuple<cv::Mat, int, int> d1, std::tuple<cv::Mat, int, int> d2)
{
    int v1 = std::get<1>(d1), v2 = std::get<1>(d2);
    if (!(v1 == v2))
        return (v1 < v2);
    else
        return ( std::get<2>(d1) <= std::get<2>(d2) );
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//////                            MAIN FUNCTION                                      //////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    auto start = std::chrono::steady_clock::now();

    // If no directories provided
    if (argc < 2)
    {
        std::cerr << "Usage: SETUP <PathToImages>\n";
        return 1;
    }

    const std::string folderOut = "./small_img/";

    // Take the given folder path
    const std::string folderPath = argv[1];

    // Only the preparation phase
    // TODO: Recognise when already done as this may take a long time
    std::vector<std::string> jpgs, jpegs, filenames;
    cv::glob( folderPath + "/*.jpg", jpgs);
    cv::glob( folderPath + "/*.jpeg", jpegs);

    
    filenames.reserve( jpegs.size() + jpgs.size() );
    for (const auto &f : jpgs)
        filenames.emplace_back(f);
    for (const auto &f : jpegs)
        filenames.emplace_back(f);

    if (filenames.size() == 0)
    {
        std::cerr << "No files found in directory: " << folderPath << '\n';
        return 1;
    }

    // Some progressbar things
    float progress = .0f; int barWidth = 50;
    // First test, pass through the files and downsize them + and put them in folder
    // Vector contains tuples of the form <image, value, hue>
    uint idx = 0;
    for (auto file : filenames)
    {
        cv::Mat img;
        img = cv::imread(file);

        // if something is larger resize it preserving the correct ratios
        resizeIfLarger(img, DST_DATA_SIZE, DST_DATA_RATIO);


        // Write the image in the given dir
        cv::imwrite(folderOut + std::to_string(idx++) + ".jpg", img);

        progress = (float)idx / (float)filenames.size();
        std::cout << '[';
        int pos = barWidth * progress;
        for (int i = 0; i < barWidth; i++)
        {
            if (i<pos) std::cout << '=';
            else if (i == pos) std::cout << '>';
            else std::cout << ' ';
        }
        std::cout << "] " << int(100.0f * progress) << "%\r";
        std::cout.flush();

    }


    auto end = std::chrono::steady_clock::now();
    std::cout << "\nConfiguration of data done, took " << std::chrono::duration_cast<std::chrono::seconds>(end-start).count() << " seconds\n";

    return 0;
}



// Take in image and max allowed size, if either width or height of img is larger
// this will resize it
void resizeIfLarger( cv::Mat &img, const int &max_size)
{

        // if something is larger resize it preserving the correct ratios
        if (img.cols > max_size || img.rows > max_size)
        {
            float ratio = (float)img.cols / (float)img.rows;

            int new_width = (ratio > 1) ? max_size : (int)(max_size * ratio);
            int new_height = (ratio < 1) ? max_size : (int)((float)max_size / ratio);
            cv::resize( img, img, cv::Size(new_width, new_height));
        }
}
// Overload, it we want to fix the width and the height
void resizeIfLarger( cv::Mat &img, const int &max_width, const int &max_height)
{

        // if something is larger resize it preserving the correct ratios
        if (img.cols > max_width || img.rows > max_height)
        {
            int new_width = (img.cols > max_width) ? max_width : img.cols;
            int new_height = (img.rows > max_height) ? max_height : img.rows;
            cv::resize( img, img, cv::Size(new_width, new_height));
        }
}
// Overload, if we want to specify destination ratio, the images will be cut from top left
// TODO: Crop it in the center
void resizeIfLarger( cv::Mat &img, const int &max_size, const float &dst_ratio)
{
    const int old_width = img.cols, old_height = img.rows;
    // first we use the ratio to cut the image
    float src_ratio = (float)old_width / (float)old_height;
    if (dst_ratio < src_ratio)
    {
        int new_width = old_width * (dst_ratio / src_ratio);
        int x = (old_width - new_width) / 2;
        img = img(cv::Rect(x,0, new_width, old_height));
    }
    else if (dst_ratio > src_ratio)
    {
        int new_height = old_height * (src_ratio / dst_ratio);
        int y = (old_height - new_height) / 2;
        img = img(cv::Rect(0,y, old_width, new_height));
    }

        // if something is larger resize it preserving the original ratio
        if (img.cols > max_size || img.rows > max_size)
        {

            int new_width = (dst_ratio > 1) ? max_size : (int)(max_size * dst_ratio);
            int new_height = (dst_ratio < 1) ? max_size : (int)((float)max_size / dst_ratio);
            cv::resize( img, img, cv::Size(new_width, new_height));
        }
}