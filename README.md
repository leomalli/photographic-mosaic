# Photographic mosaic

## Goal
The goal is to do [photographics mosaic](https://en.wikipedia.org/wiki/Photographic_mosaic).

We want the possibility for the user to give an input set of images, and a goal image.
Then the program will inspect the set of images and organize it, and then use this dataset to reconstruct the goal image given.

**The program may not modify the colors nor the values of the given input dataset**, but will surely reduced the sizes of the pictures.

At the moment, it isn't sure that each images will correspond to a "pixel", maybe that some overlaping will take place.


## Usage
First clone the repo and build it using

    mkdir build && cd build
    cmake ..
    make

The program is separated in two steps, first build the image dataset (setup.cpp), then compose the image using the previously build dataset (compose/dithered.cpp).

### Build dataset
In the build directory, just run the `SETUP` executable passing the directory containing your original image dataset.

    ./src/SETUP PathToDataSet/

Note that the script will resize and crop the image to a square format of size 200x200 pixels by default.
To alter those parameters, change the values of `DST_DATA_SIZE` and `DST_DATA_RATIO` in the `photographic-mosaic/src/setup.cpp` file.

**WARNING:** A `DST_DATA_RATIO` other that 1 result in distorded images at the moment.
I have yet to implement it correctly.


The resulting smaller images will then be saved in the build folder under `build/small_img/`.


### Compose images
You have two fairly similar executables to generate images: `COMPOSE` and `DITHERED`.

The first one will only get the closest images (based on mean RGB) from the pixel.  
The second one will use [Floyd-Steinberg dithering](https://en.wikipedia.org/wiki/Floyd%E2%80%93Steinberg_dithering) to hopefully get a more accurate result.

Note that due to the algorithm, `DITHERED` cannot run in parallel.
You will find it **way** slower than the initial `COMPOSE` executable.

When in build directory, just run

    ./src/COMPOSE PathToImg/
    ./src/DITHERED PathToImg/

to use either one of the algorithm.
The resulting image is then saved to `build/OUTPUT.jpg`.

You will find at the top of the `compose.cpp` and `dithered.cpp` files the following definitions:

    #define MAX_SIZE_INPUT 100
    #define MAX_SIZE_OUTPUT 10000
    #define MAX_DIFF 60.0f

You can modify them as you see fit to obtain different results.  
- `MAX_SIZE_INPT`:  
Size to which given image will be downsampled before beeing processed.
E.g., by default, if given a 700x1000p image, it will be downsized to a 70x100p one before processing it.
- `MAX_SIZE_OUTPUT`:  
Max length size of either width or height of the saved outputed image.
- `MAX_DIFF`:  
When computing difference between the pixel value and datasets values, we use the squared two-norm in the RGB space.
We then choose a random image whose distance to pixel is less than `MAX_DIFF`.
If no image is close enough, we choose the closest one.
